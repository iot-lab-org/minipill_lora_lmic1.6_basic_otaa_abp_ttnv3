# MiniPill LoRa Hardware Version 1.x Basic OTAA and ABP with TTNv3

Please look at https://www.iot-lab.org/blog/370/ for general information on the project with the MiniPill LoRa.
In this this file I will share some software specific information. Pay also attention to the comments in the code.

## The Things Stack V3 support
Due to changes at The Things Network, devices should be updated to support LoRaWAN MAC 1.0.2/1.0.3. With the use of the latest LMIC (version 1.6) MCCI Library for Arduino this should be accomplished. By default LMIC_LORAWAN_SPEC_VERSION_1_0_3 is selected. Some communication parameters must be added to get the communication right.

## ABP and OTAA support
Added code so you can also use ABP as activation mechanismen. Please look in the source code for detailed instructions.
In general: in main.cpp the DISABLE_JOIN is defined. Both OTAA and ABP setup are able to receive downlink messages.

## Test results TTN Stack V3 and OTAA
The early test with OTAA result in many downlink messages (2400 uplink with about 460 downlink). I guess this is not normal behaviour.
2020-03-28 added also at the end of setup function communication parameters for OTAA to prevent unwanted downlink messages.

## Basic setup
No sensor or low-power options are used. Look for other projects for code with low-power and sensor reading. All MiniPill LoRa hardware versions 1.x are supported by this code.

## Supported hardware protocols
Serial, SPI and I2C communication is now supported. To activate I2C you have to activate the peripheral clock.
Lookup the Clock Settings remark at the end of this file.

## PlatformIO
Remember that this code is used in combination with the PlatformIO toolset.
This can be found at https://platformIO.org. I use the toolset in combination with the Atom IDE on MacOS.

## Adding a custom board
To work with the MiniPill LoRa in more than one project you should add this custom board to you PlatformIO toolset.
On MacOS under the user's homedirectory a .platformio directory is available for
the toolsets. This is the platformio homedirectory. Please check for your OS where this directory is located.

- copy the *boards* and *variants* directory from customboard directory to the .platformio directory
- change the absolute path in the boards/minipill_l051c8_lora.json file for the variants path
- restart platformio/IDE

Now you should be able to use the MiniPill LoRa board in a new project.
Not all functions are tested with this custom board configurations.

## debugging
I use the hardwareSerial for debugging and connected a serial TTL-USB converter. This will take
some power in Low Power mode.

## Clock Settings
The clock settings for this custom board are set in the
`.platformio/variants/MINIPILL_L051XX_LORA/variant.cpp` file. In the `WEAK void SystemClock_Config(void)` function the HAL functions are called to select the clocks.
For use of I2C and 100kHz clock signal you have to change these settings in:

	WEAK void SystemClock_Config(void)
	{
	  RCC_OscInitTypeDef RCC_OscInitStruct = {};
	  RCC_ClkInitTypeDef RCC_ClkInitStruct = {};
	  RCC_PeriphCLKInitTypeDef PeriphClkInit = {};

	  /* Configure the main internal regulator output voltage */
	  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	  /* Initializes the CPU, AHB and APB busses clocks */
	  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
	  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	  RCC_OscInitStruct.MSICalibrationValue = 0;
	  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_5;
	  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
	    Error_Handler();
	  }

	  /* Initializes the CPU, AHB and APB busses clocks  */
	  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
	                                | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
	  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK) {
	    Error_Handler();
	  }
	  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2
	                                       | RCC_PERIPHCLK_I2C1;
	  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
	  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
	    Error_Handler();
	  }
	}

You can also **add this function to you main code** and call it in the first line of the setup function of your Arduino main program. Do not use the WEAK compiler direction.

When you must use a higher clock rate you should use the HSI clock. In my experience this is necessary when using OLED displays with I2C communication.

The function for 400kHz clock rate is:

	void SystemClock_Config(void)
	{
	  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
	  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

	  /** Configure the main internal regulator output voltage
	  */
	  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	  /** Initializes the RCC Oscillators according to the specified parameters
	  * in the RCC_OscInitTypeDef structure.
	  */
	  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  /** Initializes the CPU, AHB and APB buses clocks
	  */
	  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
	                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV8;
	  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
	  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
	  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	  {
	    Error_Handler();
	  }
	}

Remember that the change in clock settings will affect power consumption! You can generate you own clock settings by using STM32CubeMX or STM32CubeMXIDE configurator. Copy the `SystemClock_Config(void)` function generated by the configurator.
