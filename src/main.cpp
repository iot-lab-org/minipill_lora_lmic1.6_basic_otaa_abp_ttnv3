/*
  @author  Leo Korbee (c), Leo.Korbee@xs4all.nl
  @website iot-lab.org
  @license Attribution-ShareAlike 4.0 International  (CC BY-SA 4.0)

  Project MiniPill_LoRa_1.x_LMIC1.6_Basic_OTAA_ABP_TTNv3
  based on the work of T. Telkamp, M. Kooijman and T. Moore (2015/2018)
  by using the library https://github.com/mcci-catena/arduino-lmic

  This example can use
  OTAA (Over-The-Air Activation) (preferred)
  ABP (Activation-By-Personalisation)
  in combination with The Things Stack V3 (TTN)
  Using LMIC version 1.6 with LoRaWAN version MAC V1.0.3 (default)
  Look for ABP setting on iot-lab.org

  This example is build in PlatformIO IDE and hence you should change minimal
  option for other IDEs.

  Hardware
  This example is build for the MiniPill LoRa which uses a STM32L051C8T6
  microcontroller. Look at iot-lab.org for information on the hardware and
  schmatics. At the end of this file the pin layout
  This code is using about 50% of 64Kbyte flash.

  Region definitions:
  add or enable #define CFG_eu868 1 to
    MCCI_LoraWAN_LMIC_library-3.3.0/project_config/lmic_project_config.h
  using ABP instead of OTAA:
  add #define DISABLE_JOIN to the top of this file
  and add apropiate security settings in secconfig.h

  @2021-02-06
  added LMIC.rxDelay = 5; to setup for ABP to prevent downlink Rx delay messages
  (only first 1 or 2 LoRaWAN frames have them now). The default delay TTNv3 is
  5 seconds.

  @2021-03-28
  also with OTAA and TTNv3 some communication parameter must be adapted to prevent
  unwanted downlink messages

  @2021-03-28
  Added debugutils for easy switchting on and off the debugging by serial2 port
  Thanks to Haim, who gave me an example code back with this included

  @2021-03-29
  Changed Clock speed setting for better response on OTAA, with the 2 MHz settings
  it took 5 join requests to get a join. Have to measure what the impact is on Low
  Power.

*/

// disable OTAA and enable ABP by using this define
// #define DISABLE_JOIN

// set debug on by using this define
#define DEBUG

#include "debugutils.h"
#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
// include security settings. look at secconfig_example.h and rename it to
// secconfig.h
#include "secconfig.h"

#ifdef DEBUG
  // for debugging redirect to hardware Serial2
  // Tx on PA2
  HardwareSerial Serial2(USART2);   // or HardWareSerial Serial2 (PA3, PA2);
#endif

// function prototype
void do_send(osjob_t* j);

#ifdef DISABLE_JOIN
  // These callbacks are only used in over-the-air activation, so they are
  // left empty here. We cannot leave them out completely otherwise the linker will complain.
  void os_getArtEui (u1_t* buf) { }
  void os_getDevEui (u1_t* buf) { }
  void os_getDevKey (u1_t* buf) { }
#else
  void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
  void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
  void os_getDevKey (u1_t* buf) { memcpy_P(buf, APPKEY, 16);}
#endif

static uint8_t mydata[] = "AB";
static osjob_t sendjob;

// Schedule TX every this many seconds (mind the minimum time! due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 300;

// Pin mapping
const lmic_pinmap lmic_pins =
{
    .nss = PA4,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = PA9,
    .dio = {PA10, PB4, PB5},
};

void printHex2(unsigned v)
{
    v &= 0xff;
    if (v < 16)
        DEBUG_PRINT('0');
    DEBUG_PRINT(v, HEX);
}

void onEvent (ev_t ev)
{
    DEBUG_PRINT(os_getTime());
    DEBUG_PRINT(": ");
    switch(ev)
    {
        case EV_SCAN_TIMEOUT:
            DEBUG_PRINTLN(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            DEBUG_PRINTLN(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            DEBUG_PRINTLN(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            DEBUG_PRINTLN(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            DEBUG_PRINTLN(F("EV_JOINING"));
            break;
        case EV_JOINED:
            DEBUG_PRINTLN(F("EV_JOINED"));
            {
              u4_t netid = 0;
              devaddr_t devaddr = 0;
              u1_t nwkKey[16];
              u1_t artKey[16];
              LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
              DEBUG_PRINT("netid: ");
              DEBUG_PRINTLN(netid, DEC);
              DEBUG_PRINT("devaddr: ");
              DEBUG_PRINTLN(devaddr, HEX);
              DEBUG_PRINT("AppSKey: ");
              for (size_t i=0; i<sizeof(artKey); ++i)
              {
                if (i != 0)
                  DEBUG_PRINT("-");
                printHex2(artKey[i]);
              }
              DEBUG_PRINTLN("");
              DEBUG_PRINT("NwkSKey: ");
              for (size_t i=0; i<sizeof(nwkKey); ++i)
              {
                      if (i != 0)
                              DEBUG_PRINT("-");
                      printHex2(nwkKey[i]);
              }
              DEBUG_PRINTLN();
            }
            // Disable link check validation (automatically enabled
            // during join, but because slow data rates change max TX
	    // size, we don't use it in this example.
            LMIC_setLinkCheckMode(0);
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_RFU1:
        ||     DEBUG_PRINTLN(F("EV_RFU1"));
        ||     break;
        */
        case EV_JOIN_FAILED:
            DEBUG_PRINTLN(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            DEBUG_PRINTLN(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            DEBUG_PRINTLN(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              DEBUG_PRINTLN(F("Received ack"));
            if (LMIC.dataLen)
            {
              DEBUG_PRINT(F("Received "));
              DEBUG_PRINT(LMIC.dataLen);
              DEBUG_PRINTLN(F(" bytes of payload"));
            }
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            break;
        case EV_LOST_TSYNC:
            DEBUG_PRINTLN(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            DEBUG_PRINTLN(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            DEBUG_PRINTLN(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            DEBUG_PRINTLN(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            DEBUG_PRINTLN(F("EV_LINK_ALIVE"));
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_SCAN_FOUND:
        ||    DEBUG_PRINTLN(F("EV_SCAN_FOUND"));
        ||    break;
        */
        case EV_TXSTART:
            DEBUG_PRINTLN(F("EV_TXSTART"));
            break;
        case EV_TXCANCELED:
            DEBUG_PRINTLN(F("EV_TXCANCELED"));
            break;
        case EV_RXSTART:
            /* do not print anything -- it wrecks timing */
            break;
        case EV_JOIN_TXCOMPLETE:
            DEBUG_PRINTLN(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
            break;
        default:
            DEBUG_PRINT(F("Unknown event: "));
            DEBUG_PRINTLN((unsigned) ev);
            break;
    }
}

void do_send(osjob_t* j)
{
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND)
    {
        DEBUG_PRINTLN(F("OP_TXRXPEND, not sending"));
    }
    else
    {
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, mydata, sizeof(mydata)-1, 0);
        DEBUG_PRINTLN(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}


void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_HSI;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}




void setup()
{
    SystemClock_Config();
    Serial.begin(9600);
    DEBUG_PRINTLN(F("Starting"));

    // to increse the size of the RX window.
    LMIC_setClockError(MAX_CLOCK_ERROR * 10 / 100);

    // Set static session parameters when using ABP.
    // Instead of dynamically establishing a session
    // by connecting to the network, precomputed session parameters are provided.
    #ifdef DISABLE_JOIN
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #endif

    // These settings are needed for correct communication, on TTNv3 you get
    // unwanted downlink messages
    // Disable ADR
    LMIC_setAdrMode(0);
    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF9;
    // essential for TTNv3, otherwise you will get a Scheduled downlink message
    // Rx1 delay: 5 or other number depending on the config
    LMIC.rxDelay = 5;
    // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
    LMIC_setDrTxpow(DR_SF7,14);

    // Real Time Operating System part init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();

    // Start job (sending automatically starts OTAA too)
    do_send(&sendjob);
}

void loop()
{
    os_runloop_once();
}

/* MiniPill LoRa v1.x mapping - LoRa module RFM95W

  PA4  // SPI1_NSS   NSS  - RFM95W
  PA5  // SPI1_SCK   SCK  - RFM95W
  PA6  // SPI1_MISO  MISO - RFM95W
  PA7  // SPI1_MOSI  MOSI - RFM95W

  PA10 // USART1_RX  DIO0 - RFM95W
  PB4  //            DIO1 - RFM95W
  PB5  //            DIO2 - RFM95W

  PA9  // USART1_TX  RST  - RFM95W

                     VCC - RFM95W 3V3
                     GND - RFM95W GND
*/
