/*
  secconfig_example.h
  configuration file with security information.

  @author  Leo Korbee (c), Leo.Korbee@xs4all.nl
  @website iot-lab.org
  @license Attribution-ShareAlike 4.0 International  (CC BY-SA 4.0)

  please configure/enable one of these security settings by removing or
  adding comment marks

  please rename this file to secconfig.h
*/

#ifdef DISABLE_JOIN
// ABP security settings  =====================================================

// LoRaWAN NwkSKey, network session key
// This should be in big-endian (aka msb).
//static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

// LoRaWAN AppSKey, application session key
// This should also be in big-endian (aka msb).
//static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

// LoRaWAN end-device address (DevAddr)
// The library converts the address to network byte order as needed,
// so this should be in big-endian (aka msb) too.
//static const u4_t DEVADDR = 0x00000000 ;

#else
// OTAA security settings =====================================================

// This EUI must be in little-endian format (aka lsb), so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes.
static const u1_t PROGMEM APPEUI[8]={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

// This should also be in little endian format (aka lsb), see above.
static const u1_t PROGMEM DEVEUI[8]={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

// This key should be in big endian format (aka msb) (or, since it is not really a
// number but a block of memory, endianness does not really apply).
static const u1_t PROGMEM APPKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

#endif
